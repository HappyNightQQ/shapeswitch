using System;
using ObstacleManagement;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

public class PlayerManager : MonoBehaviour
{
    public delegate void DieEvent();
    public static event DieEvent OnDeath;
    public GameObject[] shapes;

    public Shape CurrShape
    {
        get => _currShape;
        set
        {
            shapes[(int)_currShape].SetActive(false);
            _currShape = value;
            shapes[(int)value].SetActive(true);
        }
    }

    private Shape _currShape = Shape.Square;

    void Awake()
    {
        CurrShape = (Shape) Random.Range(0, 2);
    }

    public void SwitchToCircle()
    {
        CurrShape = Shape.Circle;
    }
        
    public void SwitchToSquare()
    {
        CurrShape = Shape.Square;
    }
        
    public void SwitchToTriangle()
    {
        CurrShape = Shape.Triangle;
    }
    
    public void CheckCollision(Shape shape)
    {
        if (CurrShape == shape)
            return;

        OnDeath?.Invoke();
    }

    public void OnGameStart()
    {
        CurrShape = Shape.Square;
    }

    private void OnEnable()
    {
        GameManager.OnGameStart += OnGameStart;
        ObstacleManager.OnObstacleCollisionCheck += CheckCollision;
    }

    private void OnDisable()
    {
        GameManager.OnGameStart -= OnGameStart;
        ObstacleManager.OnObstacleCollisionCheck -= CheckCollision;
    }
}