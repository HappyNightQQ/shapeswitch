using System;
using System.Collections;
using System.Collections.Generic;
using ObstacleManagement;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    //TODO:: USE GETTER AND SETTER TO UPDATE TEXT BOX
    private TextMeshProUGUI _scoreTextBox;
    private int _score = 0;

    private void Awake()
    {
        _scoreTextBox = transform.Find("ScoreValue").gameObject.GetComponent<TextMeshProUGUI>();
        ChangeScore(0);
    }

    public void ChangeScore(int score)
    {
        Assert.IsFalse(score < 0,  "Something is wrong with the score");

        _score = score;
        _scoreTextBox.text = score.ToString();
    }

    public void IncreaseScore()
    {
        ++_score;
        _scoreTextBox.text = _score.ToString();
    }

    public void OnGameStart()
    {
        _score = 0;
        _scoreTextBox.text = _score.ToString();
    }
    
    private void OnEnable()
    {
        GameManager.OnGameStart += OnGameStart;
        ObstacleManager.OnObstaclePassed += IncreaseScore;
    }

    private void OnDisable()
    {
        GameManager.OnGameStart -= OnGameStart;
        ObstacleManager.OnObstaclePassed -= IncreaseScore;
    }
}
