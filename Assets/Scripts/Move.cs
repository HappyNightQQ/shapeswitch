using UnityEditor.UIElements;
using UnityEngine;

public class Move : MonoBehaviour
{
    public bool canMove;
    public int speed;

    void Update()
    {
        if (!canMove)
        {
            return;
        }

        transform.position += transform.right * Time.deltaTime * speed;
    }

    public void OnPlayerDeath()
    {
        canMove = false;
    }

    public void OnGameStart()
    {
        transform.position = Vector3.zero;
        canMove = true;
    }

    public void OnGameContinueAfterDeath()
    {
        canMove = true;
    }
    
    private void OnEnable()
    {
        GameManager.OnGameStart += OnGameStart;
        GameManager.OnGameContinueAfterDeath += OnGameContinueAfterDeath;
        PlayerManager.OnDeath += OnPlayerDeath;
    }

    private void OnDisable()
    {
        GameManager.OnGameStart -= OnGameStart;
        GameManager.OnGameContinueAfterDeath -= OnGameContinueAfterDeath;
        PlayerManager.OnDeath -= OnPlayerDeath;
    }
}