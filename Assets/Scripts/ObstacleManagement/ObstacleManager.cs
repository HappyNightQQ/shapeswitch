using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Utils;

namespace ObstacleManagement
{
    public class ObstacleManager : MonoBehaviour
    {
        public delegate void SpawnEvent();
        public static event SpawnEvent OnObstaclePassed;
        public delegate void SpawnEventWithShape(Shape shape);
        public static event SpawnEventWithShape OnObstacleCollisionCheck;
        
        [SerializeField] private ObstacleData[] obstacles;
        [SerializeField] private GameObject obstacleSpawnPoint;
        [SerializeField] private GameObject player;
        private const int NumberOfObstaclesOnScreen = 10;
        private const int DistanceBetweenObstacles = 30;
        private ObstacleFactory _obstacleFactory;
        private readonly Queue<ObstacleData> _currObstacles = new Queue<ObstacleData>(NumberOfObstaclesOnScreen);
        private readonly Vector3 _obstacleStartPosition = new Vector3(30,0,0);
        private bool gameRunning = false;
        
        public void Awake()
        {
            Assert.IsNotNull(obstacleSpawnPoint, "There is not obstacle spawn point");
            
            _obstacleFactory = new ObstacleFactory(obstacles);
        }

        public void Update()
        {
            if (!gameRunning) return;
            if (player.transform.position.x < _currObstacles.Peek().gameObject.transform.position.x) return;
            
            OnObstacleCollisionCheck?.Invoke(_currObstacles.Peek().shape);
            
            var pooledObject = _currObstacles.Dequeue();
            pooledObject.gameObject.SetActive(false);
            _obstacleFactory.ReturnObstacle(pooledObject.gameObject);
            OnObstaclePassed?.Invoke();
        }
        
        public void Spawn(int numberOfObstacles)
        {
            for (int i = 0; i < numberOfObstacles; ++i)
            {
                Spawn();
            }
        }

        public void Spawn()
        {
            var spawned = _obstacleFactory.SpawnRandomObstacle(obstacleSpawnPoint.transform.position, transform);
            obstacleSpawnPoint.transform.Translate(DistanceBetweenObstacles, 0,0);
            _currObstacles.Enqueue(spawned);
        }

        public Shape PeekShape()
        {
            return _currObstacles.Peek().shape;
        }

        public void OnGameStart()
        {
            RecycleObstacles();
            obstacleSpawnPoint.transform.position = _obstacleStartPosition;
            Spawn(NumberOfObstaclesOnScreen);
            gameRunning = true;
        }
        
        public void RecycleObstacles()
        {
            while (_currObstacles.Count > 0)
            {
                _obstacleFactory.ReturnObstacle(_currObstacles.Dequeue().gameObject);
            }
        }

        public void OnGameEnd()
        {
            gameRunning = false;
        }
        
        public void OnGameContinueAfterDeath()
        {
            gameRunning = true;
        }

        private void OnEnable()
        {
            GameManager.OnGameStart += OnGameStart;
            GameManager.OnGameContinueAfterDeath += OnGameContinueAfterDeath;
            PlayerManager.OnDeath += OnGameEnd;
            OnObstaclePassed += Spawn;
        }

        private void OnDisable()
        {
            GameManager.OnGameStart -= OnGameStart;
            GameManager.OnGameContinueAfterDeath -= OnGameContinueAfterDeath;
            PlayerManager.OnDeath -= OnGameEnd;
            OnObstaclePassed -= Spawn;
        }
    }
}
