using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using Shape = Utils.Shape;

namespace ObstacleManagement
{
    public class ObstacleFactory
    {
        private readonly Dictionary<Shape, GameObject> _obstaclesPrefabs;
        private readonly ObstaclePool _obstaclePool = new ObstaclePool();

        public ObstacleFactory(ObstacleData[] obstacles)
        {
            Assert.IsFalse(null == obstacles || obstacles.Length == 0, "Obstacles were not assigned");
            
            _obstaclesPrefabs = new Dictionary<Shape, GameObject>(obstacles.Length);
            foreach (var obstacle in obstacles)
            {
                _obstaclesPrefabs.Add(obstacle.shape, obstacle.gameObject);
            }
        }

        public ObstacleData SpawnRandomObstacle(Vector3 spawnPosition, Transform parent)
        {
            Shape shape = (Shape)Random.Range(0, _obstaclesPrefabs.Count);
            
            Debug.Log("Spawned " + shape);
            
            if (!_obstaclePool.HasElement(shape))
            {
                GameObject created = Object.Instantiate(_obstaclesPrefabs[shape], parent);
                _obstaclePool.Add(shape, created);
            }

            var spawned = _obstaclePool.Remove(shape);
            #if UNITY_EDITOR
            // _obstaclePool.PrintCounts();
            #endif
            spawned.transform.position = spawnPosition;
            spawned.SetActive(true);
            return new ObstacleData(spawned, shape);
        }

        public void ReturnObstacle(GameObject obstacle)
        {
            if (null == obstacle) return;

            Shape shape = 0;
            if (obstacle.name.Contains("Circle"))
            {
                shape = Shape.Circle;
            }
            else if (obstacle.name.Contains("Square"))
            {
                shape = Shape.Square;
            }
            else if (obstacle.name.Contains("Triangle"))
            {
                shape = Shape.Triangle;
            }
            obstacle.SetActive(false);
            _obstaclePool.Add(shape, obstacle);
        }

        class ObstaclePool
        {
            private readonly Dictionary<Shape, Queue<GameObject>> _pool
                = new Dictionary<Shape, Queue<GameObject>>();

            public void Add(Shape shape,GameObject obj)
            {
                if (!_pool.ContainsKey(shape))
                {
                    _pool.Add(shape, new Queue<GameObject>());
                }
                
                _pool[shape].Enqueue(obj);
            }

            public GameObject Remove(Shape shape)
            {
                if (!_pool.ContainsKey(shape))
                {
                    return null;
                }

                return _pool[shape].Dequeue();
            }

            public bool HasElement(Shape shape)
            {
                if (!_pool.ContainsKey(shape))
                {
                    return false;
                }

                return _pool[shape].Count != 0;
            }
            
            #if UNITY_EDITOR
            public void PrintCounts()
            {
                if (!_pool.ContainsKey(Shape.Circle) || !_pool.ContainsKey(Shape.Square) || !_pool.ContainsKey(Shape.Triangle))
                {
                    return;
                }
                Debug.Log("Shape: " + Shape.Circle + " size: " +  _pool[Shape.Circle].Count);
                Debug.Log("Shape: " + Shape.Square +  " size: " +  _pool[Shape.Square].Count);
                Debug.Log("Shape: " + Shape.Triangle +  " size: " +  _pool[Shape.Triangle].Count);
            }
            #endif
        }
    }
}
