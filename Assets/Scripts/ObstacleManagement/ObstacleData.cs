using UnityEngine;
using Utils;

namespace ObstacleManagement
{
    [System.Serializable]
    public class ObstacleData
    {
        public GameObject gameObject;// { get; set; }
        public Shape shape;// { get; set; }
            
        public ObstacleData(GameObject gameObject, Shape shape)
        {
            this.gameObject = gameObject;
            this.shape = shape;
        }
    }
}