using System;
using System.Collections;
using System.Collections.Generic;
using ObstacleManagement;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

public class GameManager : MonoBehaviour
{
    private const int SecondsToWaitForContinueAfterDeath = 5;
    
    public delegate void GameStartEvent();
    public static event GameStartEvent OnGameStart;
    public static event GameStartEvent OnGameContinueAfterDeath;

    [SerializeField] private GameObject deathScreen;
    [SerializeField] private GameObject buttonsUI;
    [SerializeField] private GameObject mainMenuUI;
    [SerializeField] private ScoreManager scoreManager;


    private Coroutine _deathContinueCoroutine = null;
    private TextMeshProUGUI _continueTimerText;
    private void Awake()
    {
        Assert.IsNotNull(deathScreen, "There is no Death Screen");
        _continueTimerText = deathScreen.transform.Find("Continue").Find("ContinueTimer").GetComponent<TextMeshProUGUI>();
        deathScreen.SetActive(false);
        buttonsUI.SetActive(false);
        mainMenuUI.SetActive(true);
    }

    public void OnPlayerDeath()
    {
        Debug.Log("Dead");
        _continueTimerText.text = SecondsToWaitForContinueAfterDeath.ToString();
        buttonsUI.SetActive(false);
        deathScreen.SetActive(true);
        _deathContinueCoroutine = StartCoroutine(AfterDeathRoutine());
    }

    public IEnumerator AfterDeathRoutine()
    {
        int timeLeft = int.Parse(_continueTimerText.text);
        while (timeLeft > 0)
        {
            yield return new WaitForSeconds(1);
            --timeLeft;
            _continueTimerText.text = timeLeft.ToString();
        }
        
        //TODO:: To fix no continue I have to turn this on when pressing continue
        deathScreen.transform.Find("Continue").gameObject.SetActive(false);
    }

    public void StartGame()
    {
        if (null != _deathContinueCoroutine)
        {
            StopCoroutine(_deathContinueCoroutine);
            deathScreen.transform.Find("Continue").gameObject.SetActive(true);
        }
        mainMenuUI.SetActive(false);
        buttonsUI.SetActive(true);
        OnGameStart?.Invoke();
    }

    public void TryContinue()
    {
        if (null != _deathContinueCoroutine)
        {
            StopCoroutine(_deathContinueCoroutine);
            deathScreen.transform.Find("Continue").gameObject.SetActive(true);
        }
        deathScreen.SetActive(false);
        buttonsUI.SetActive(true);
        OnGameContinueAfterDeath?.Invoke();
    }

    public void Restart()
    {
        deathScreen.SetActive(false);
        buttonsUI.SetActive(false);
        StartGame();
    }

    private void OnEnable()
    {
        PlayerManager.OnDeath += OnPlayerDeath;
    }

    private void OnDisable()
    {
        PlayerManager.OnDeath -= OnPlayerDeath;
    }
}
