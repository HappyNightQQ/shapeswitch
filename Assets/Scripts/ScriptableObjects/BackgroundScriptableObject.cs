using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "BackgroundScriptableObject", menuName = "Scriptable Objects/Background")]
    public class BackgroundScriptableObject : ScriptableObject
    {
        public GameObject[] backgrounds;
    }
}
