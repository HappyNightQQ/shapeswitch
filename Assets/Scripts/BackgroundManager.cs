using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class BackgroundManager : MonoBehaviour
{
    private Transform _followObjectTransform;

    private BackgroundQueue _backgroundQueue;

    private int CameraOffset => 5;
    private int CurrentBackgroundLength 
        => (int) _backgroundQueue.PeekCurrentBackground().transform.localScale.x;

    void Awake()
    {
        _followObjectTransform = GameObject.FindWithTag("Player").transform;

        Assert.IsNotNull(_followObjectTransform, "There is no object for the background to follow");
        if (null == _followObjectTransform)
        {
            Debug.LogError("");
            return;
        }
            
        _backgroundQueue = GetComponent<BackgroundQueue>();
    }
        
    void Update()
    {
        GameObject currentBackground = _backgroundQueue.PeekCurrentBackground();
        if (_followObjectTransform.position.x - currentBackground.transform.position.x <
            CurrentBackgroundLength + CameraOffset)
        {
            return;
        }

        _backgroundQueue.SwitchToNextBackground();
    }
}