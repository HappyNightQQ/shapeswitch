using System;

namespace Utils
{
    [Serializable]
    public enum Shape
    {
        Square,
        Triangle,
        Circle
    }
}
