﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using ScriptableObjects;
using UnityEngine;

public class BackgroundQueue : MonoBehaviour
{
    public BackgroundScriptableObject backgroundScriptableObject;

    private Vector3 _nextSpawnPosition = Vector3.zero;
        
    private readonly Queue<GameObject> _backgrounds = new Queue<GameObject>();
    private readonly Dictionary<GameObject, float> _backgroundsLength = new Dictionary<GameObject, float>();

    public void Awake()
    {
        foreach (var background in backgroundScriptableObject.backgrounds)
        {
            GameObject nextAdd = Instantiate(background, _nextSpawnPosition, Quaternion.Euler(-90,0,0));
            _backgroundsLength.Add(nextAdd, background.GetComponent<MeshRenderer>().bounds.size.x);
            _nextSpawnPosition.x += _backgroundsLength[nextAdd];
                
            _backgrounds.Enqueue(nextAdd);
        }
    }

    public GameObject PeekCurrentBackground()
    {
        return _backgrounds.Peek();
    }

    public void SwitchToNextBackground()
    {
        GameObject currBackground = _backgrounds.Dequeue();
        currBackground.transform.position = _nextSpawnPosition;
        _nextSpawnPosition.x += _backgroundsLength[currBackground];
        _backgrounds.Enqueue(currBackground);
    }
}